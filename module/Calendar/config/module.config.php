<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Calendar\Controller\Calendar' => 'Calendar\Controller\CalendarController',
            'Calendar\Controller\Event' => 'Calendar\Controller\EventController',
        ),
    ),
	
	'router' => array(
        'routes' => array(
            'calendar' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/calendar[/:action][/:id]',
                    'constraints' => array(
                        'action'   => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'       => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Calendar\Controller\Calendar',
                        'action'     => 'index',
                    ),
                ),
            ),
			'event' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/event[/:action][/:id][/:unixTime][/:allDay]',
                    'constraints' => array(
                        'action'   => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'       => '[0-9]+',
						'unixTime' => '[0-9]+',
						'allDay'   => '0|1',
                    ),
                    'defaults' => array(
                        'controller' => 'Calendar\Controller\Event',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
	
    'view_manager' => array(
        'template_path_stack' => array(
            'calendar' => __DIR__ . '/../view',
        ),
    ),
);
