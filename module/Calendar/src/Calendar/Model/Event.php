<?php
	namespace Calendar\Model;

	use Zend\InputFilter\Factory as InputFactory;
	use Zend\InputFilter\InputFilter;
	use Zend\InputFilter\InputFilterAwareInterface;
	use Zend\InputFilter\InputFilterInterface;
	use Zend\Validator\Date;

	class Event implements InputFilterAwareInterface
	{
		public $event_id;
		public $calendar_id;
		public $author_id;
		public $title;
		public $description;
		public $begin;
		public $end;
		public $author_email;
		public $calendar_title;
		
		protected $inputFilter;

		public function exchangeArray($data)
		{
			$this->event_id     = (isset($data['event_id'])) ? $data['event_id'] : null;
			$this->calendar_id  = (isset($data['calendar_id'])) ? $data['calendar_id'] : null;
			$this->author_id    = (isset($data['author_id'])) ? $data['author_id'] : null;
			$this->title        = (isset($data['title'])) ? $data['title'] : null;
			$this->description  = (isset($data['description'])) ? $data['description'] : null;
			$this->begin        = (isset($data['begin'])) ? $data['begin'] : null;
			$this->end          = (isset($data['end'])) ? $data['end'] : null;
			$this->author_email = (isset($data['author_email'])) ? $data['author_email'] : null;
			$this->calendar_title = (isset($data['calendar_title'])) ? $data['calendar_title'] : null;
		}
		
		public function getArrayCopy()
		{
			return get_object_vars($this);
		}
		
		// Add content to this method:
		public function setInputFilter(InputFilterInterface $inputFilter)
		{
			throw new \Exception("Not used");
		}

		public function getInputFilter()
		{
			if (!$this->inputFilter) {
				$inputFilter = new InputFilter();
				$factory     = new InputFactory();

				$inputFilter->add($factory->createInput(array(
					'name'     => 'event_id',
					'required' => true,
					'filters'  => array(
						array('name' => 'Int'),
					),
				)));
				
				$inputFilter->add($factory->createInput(array(
					'name'     => 'calendar_id',
					'required' => true,
					'filters'  => array(
						array('name' => 'Int'),
					),
				)));
				
				$inputFilter->add($factory->createInput(array(
					'name'     => 'author_id',
					'required' => true,
					'filters'  => array(
						array('name' => 'Int'),
					),
				)));

				$inputFilter->add($factory->createInput(array(
					'name'     => 'title',
					'required' => true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 1,
								'max'      => 50,
							),
						),
					),
				)));

				$inputFilter->add($factory->createInput(array(
					'name'     => 'description',
					'required' => false,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 1,
								'max'      => 500,
							),
						),
					),
				)));
				
				$inputFilter->add($factory->createInput(array(
					'name'     => 'begin',
					'required' => true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name' => 'Date',
							'options' => array(
								'format' => 'Y-m-d H:i:s',
							),
						),
					),
				)));
				
				$inputFilter->add($factory->createInput(array(
					'name'     => 'end',
					'required' => true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name' => 'Date',
							'options' => array(
								'format' => 'Y-m-d H:i:s',
							),
						),
						array(
                        'name' => 'Callback',
							'options' => array(
								'messages' => array(
										\Zend\Validator\Callback::INVALID_VALUE => 'The end date should be greater than start date',
								),
								'callback' => function($value, $context = array()) {                                    
									$begin = \DateTime::createFromFormat('Y-m-d H:i:s', $context['begin']);
									$end = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
									return $end > $begin;
								},
							),
						),     
					),
				)));

				$this->inputFilter = $inputFilter;
			}

			return $this->inputFilter;
		}
	}
	