<?php
	namespace Calendar\Model;

	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\Sql\Where;

	class CalendarTable
	{
		protected $tableGateway;

		public function __construct(TableGateway $tableGateway)
		{
			$this->tableGateway = $tableGateway;
		}
		
		protected function resultToObjects($resultSet)
		{
			$result = array();
			
			foreach ($resultSet as $calendar)
			{
				$c = new Calendar();
				$c->exchangeArray($calendar);
				$result[] = $c;
			}
			
			return $result;
		}

		public function fetchAll()
		{
			$sql = new Sql( $this->tableGateway->adapter ) ;
			
			$select = $sql->select() ;
			$select -> from ( $this->tableGateway->getTable() )
					-> join ( 'user' , 'user.user_id = calendar.user_id', array(
						'email'
					));
			
			$statement = $sql->prepareStatementForSqlObject($select);
			$resultSet = $statement->execute();
			
			return $this->resultToObjects($resultSet);
		}
		
		public function fetchAllByUser($user_id)
		{
			$id  = (int) $user_id;
			$sql = new Sql($this->tableGateway->adapter) ;
			$where = new Where() ;
			$where -> equalTo('calendar.user_id', $id) ;
			
			$select = $sql->select() ;
			$select -> from ($this->tableGateway->getTable())
					-> join ('user' , 'user.user_id = calendar.user_id', array(
						'email'
					))
					-> where($where);
			
			$statement = $sql->prepareStatementForSqlObject($select);
			$resultSet = $statement->execute();
			
			if ($resultSet->count() == 0)
			{
				return null;
			}
			
			return $this->resultToObjects($resultSet);
		}

		public function getCalendar($id)
		{
			$id  = (int) $id;
			$sql = new Sql($this->tableGateway->adapter) ;
			$where = new Where() ;
			$where -> equalTo('calendar.calendar_id', $id) ;
			
			$select = $sql->select() ;
			$select -> from ($this->tableGateway->getTable())
					-> join ('user' , 'user.user_id = calendar.user_id', array(
						'email'
					))
					->where($where);
			
			$statement = $sql->prepareStatementForSqlObject($select);
			$resultSet = $statement->execute();
			
			$row = $resultSet->current();
			if (!$row) {
				throw new \Exception("Nie znaleziono kalendarza o id = $id");
			}
			
			$c = new Calendar();
			$c->exchangeArray($row);
			
			return $c;
		}

		public function saveCalendar(Calendar $calendar)
		{
			$data = array(
				'calendar_id' => $calendar->calendar_id,
				'user_id'  => $calendar->user_id,
				'title' => $calendar->title,
				'description' => $calendar->description
			);

			$id = (int)$calendar->calendar_id;
			if ($id == 0) {
				$this->tableGateway->insert($data);
			} else {
				if ($this->getCalendar($id)) {
					$this->tableGateway->update($data, array('calendar_id' => $id));
				} else {
					throw new \Exception('Nie istnieje kalendarz o takim id');
				}
			}
		}

		public function deleteCalendar($id)
		{
			$this->tableGateway->delete(array('calendar_id' => $id));
		}
	}