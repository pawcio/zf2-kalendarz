<?php
	namespace Calendar\Model;

	use Zend\InputFilter\Factory as InputFactory;
	use Zend\InputFilter\InputFilter;
	use Zend\InputFilter\InputFilterAwareInterface;
	use Zend\InputFilter\InputFilterInterface;

	class Calendar implements InputFilterAwareInterface
	{
		public $calendar_id;
		public $user_id;
		public $title;
		public $description;
		public $email;
		
		protected $inputFilter;

		public function exchangeArray($data)
		{
			$this->calendar_id   = (isset($data['calendar_id'])) ? $data['calendar_id'] : null;
			$this->user_id       = (isset($data['user_id'])) ? $data['user_id'] : null;
			$this->title        = (isset($data['title'])) ? $data['title'] : null;
			$this->description  = (isset($data['description'])) ? $data['description'] : null;
			$this->email  = (isset($data['email'])) ? $data['email'] : null;
		}
		
		public function getArrayCopy()
		{
			return get_object_vars($this);
		}
		
		// Add content to this method:
		public function setInputFilter(InputFilterInterface $inputFilter)
		{
			throw new \Exception("Not used");
		}

		public function getInputFilter()
		{
			if (!$this->inputFilter) {
				$inputFilter = new InputFilter();
				$factory     = new InputFactory();

				$inputFilter->add($factory->createInput(array(
					'name'     => 'calendar_id',
					'required' => true,
					'filters'  => array(
						array('name' => 'Int'),
					),
				)));
				
				$inputFilter->add($factory->createInput(array(
					'name'     => 'user_id',
					'required' => true,
					'filters'  => array(
						array('name' => 'Int'),
					),
				)));

				$inputFilter->add($factory->createInput(array(
					'name'     => 'title',
					'required' => true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 1,
								'max'      => 100,
							),
						),
					),
				)));

				$inputFilter->add($factory->createInput(array(
					'name'     => 'description',
					'required' => false,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min'      => 1,
								'max'      => 1000,
							),
						),
					),
				)));

				$this->inputFilter = $inputFilter;
			}

			return $this->inputFilter;
		}
	}
	