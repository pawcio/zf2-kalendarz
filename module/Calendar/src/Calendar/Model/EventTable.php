<?php
	namespace Calendar\Model;

	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\Sql\Where;

	class EventTable
	{
		protected $tableGateway;

		public function __construct(TableGateway $tableGateway)
		{
			$this->tableGateway = $tableGateway;
		}
		
		protected function resultToObjects($rowSet)
		{
			$result = array();
			
			foreach ($rowSet as $event)
			{
				$result[] = $event;
			}
			
			return $result;
		}

		public function fetchAll()
		{
			$sql = new Sql( $this->tableGateway->adapter ) ;
			$select = $sql->select() ;
			$select -> from ( $this->tableGateway->getTable() )
					-> join ( array('c' => 'calendar'),
						'event.calendar_id = c.calendar_id', array(
						'calendar_title' => 'title' 
					))
					-> join ( array('u' => 'user'),
						'event.author_id = u.user_id', array(
						'author_email' => 'email'
					));
			
			$statement = $sql->prepareStatementForSqlObject($select);
			$resultSet = $statement->execute();
			
			return $this->resultToObjects($resultSet);
		}
		
		public function fetchAllByCalendar($calendar_id)
		{
			$id  = (int) $calendar_id;
			$sql = new Sql( $this->tableGateway->adapter ) ;
			$where = new Where() ;
			$where -> equalTo('event.calendar_id', $id) ;
			
			$select = $sql->select() ;
			$select -> from ( $this->tableGateway->getTable() )
					-> join ( array('c' => 'calendar'),
						'event.calendar_id = c.calendar_id', array(
						'calendar_title' => 'title' 
					))
					-> join ( array('u' => 'user'),
						'event.author_id = u.user_id', array(
						'author_email' => 'email'
					))
					->where($where);
			
			$statement = $sql->prepareStatementForSqlObject($select);
			$resultSet = $statement->execute();
			
			if ($resultSet->count() == 0)
			{
				return null;
			}
			
			return $this->resultToObjects($resultSet);
		}

		public function getEvent($id)
		{
			
//			$rowset = $this->tableGateway->select(array('event_id' => $id));
//			$row = $rowset->current();
//			if (!$row) {
//				throw new \Exception("Nie znaleziono zdarzenia o id = $id");
//			}
//			return $row;
			
			
			$id  = (int) $id;
			$sql = new Sql( $this->tableGateway->adapter ) ;
			$where = new Where() ;
			$where -> equalTo('event_id', $id) ;
			
			$select = $sql->select() ;
			$select -> from ( $this->tableGateway->getTable() )
					-> join ( array('c' => 'calendar'),
						'event.calendar_id = c.calendar_id', array(
						'calendar_title' => 'title' 
					))
					-> join ( array('u' => 'user'),
						'event.author_id = u.user_id', array(
						'author_email' => 'email'
					))
					->where($where);
			
			$statement = $sql->prepareStatementForSqlObject($select);
			$resultSet = $statement->execute();
			
			$row = $resultSet->current();
			if (!$row) {
				throw new \Exception("Nie znaleziono zdarzenia o id = $id");
			}
			
			$e = new Event();
			$e->exchangeArray($row);
			return $e;
		}

		public function saveEvent(Event $event)
		{
			$data = array(
				'event_id' => $event->event_id,
				'calendar_id' => $event->calendar_id,
				'author_id'  => $event->author_id,
				'title' => $event->title,
				'description' => $event->description,
				'begin' => $event->begin,
				'end' => $event->end
			);

			$id = (int)$event->event_id;
			if ($id == 0) {
				$this->tableGateway->insert($data);
			} else {
				if ($this->getEvent($id)) {
					$this->tableGateway->update($data, array('event_id' => $id));
				} else {
					throw new \Exception('Nie istnieje zdarzenie o takim id');
				}
			}
		}

		public function deleteEvent($id)
		{
			$this->tableGateway->delete(array('event_id' => $id));
		}
	}