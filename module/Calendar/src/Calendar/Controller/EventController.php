<?php

	namespace Calendar\Controller;

	use Zend\Mvc\Controller\AbstractActionController;
	use Zend\View\Model\ViewModel;
	use Calendar\Model\Event;
	use Calendar\Form\EventForm;
	
	class EventController extends AbstractActionController
	{
		protected $eventTable;
		
		public function getEventTable()
		{
			if (!$this->eventTable) {
				$sm = $this->getServiceLocator();
				$this->eventTable = $sm->get('Calendar\Model\EventTable');
			}
			return $this->eventTable;
		}
	
		public function createAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				// pobierz id kalendarza, do którego będzie dodany event
				//echo (int)$this->params()->fromRoute('id', 0);
				//return;
				$calendar_id = (int) $this->params()->fromRoute('id', 0);
				if ($calendar_id == 0) {
					return $this->redirect()->toRoute('calendar', array(
						'action' => 'index'
					));
				}
				
				$begin = (int) $this->params()->fromRoute('unixTime', 0);
				
				
				$form = new EventForm();
				$form->get('author_id')->setValue($this->zfcUserAuthentication()->getIdentity()->getId());
				$form->get('calendar_id')->setValue($calendar_id);
				
				if ($begin)
				{
					$form->get('begin')->setValue(date('Y-m-d H:i:s', $begin));
				}
				
				$form->get('submit')->setValue('Dodaj');

				$request = $this->getRequest();
				if ($request->isPost()) {
					$event = new Event();
					$form->setInputFilter($event->getInputFilter());
					$form->setData($request->getPost());

					if ($form->isValid()) {
						$event->exchangeArray($form->getData());
						$this->getEventTable()->saveEvent($event);

						return $this->redirect()->toRoute('calendar', array(
							'action' => 'show',
							'id' => $calendar_id
						));
					}
				}
				return array('form' => $form);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}
		
		// Zwraca dane w formacie JSON do skryptu wyświetlającego kalendarz
		public function loadAction()
		{
			$response = $this->getResponse();
			
			//if ($request->isPost()) {
				$id = (int) $this->params()->fromRoute('id', 0);
				if ($id) {
					$events = $this->getEventTable()->fetchAllByCalendar($id);
					$response->setContent(\Zend\Json\Json::encode($events));
					
				}
			//}
			
			return $response;
		}
		
		public function showAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				$id = (int) $this->params()->fromRoute('id', 0);
				if (!$id) {
					return $this->redirect()->toRoute('calendar');
				}
				
				$event = $this->getEventTable()->getEvent($id);
				
				return array('event' => $event);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}

		public function editAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				$id = (int) $this->params()->fromRoute('id', 0);
				if (!$id) {
					return $this->redirect()->toRoute('calendar');
				}
				$event = $this->getEventTable()->getEvent($id);

				$form  = new EventForm();
				$form->bind($event);
				$form->get('submit')->setAttribute('value', 'Zapisz');

				$request = $this->getRequest();
				
				if ($request->isPost()) {
					$form->setInputFilter($event->getInputFilter());
					$form->setData($request->getPost());

					if ($form->isValid()) {
						$this->getEventTable()->saveEvent($form->getData());

						return $this->redirect()->toRoute('calendar', array('action' => 'show', 'id' => $form->get('calendar_id')->getValue()));
					}
				}

				return array(
					'id' => $id,
					'form' => $form,
				);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}

		public function deleteAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				$request = $this->getRequest();
				
				
				
				$id = (int) $this->params()->fromRoute('id', 0);
				if (!$id) {
					return $this->redirect()->toRoute('calendar', array('action' => 'show', 'id' => (int) $request->getPost('calendar_id')));
				}

				if ($request->isPost()) {
					$del = $request->getPost('del', 'Nie');

					if ($del == 'Tak') {
						$id = (int) $request->getPost('event_id');
						$this->getEventTable()->deleteEvent($id);
					}

					return $this->redirect()->toRoute('calendar', array('action' => 'show', 'id' => (int) $request->getPost('calendar_id')));
				}

				return array(
					'id'    => $id,
					'event' => $this->getEventTable()->getEvent($id)
				);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}
	}