<?php
	namespace Calendar\Controller;

	use Zend\Mvc\Controller\AbstractActionController;
	use Zend\View\Model\ViewModel;
	use Calendar\Model\Calendar;
	use Calendar\Form\CalendarForm;
	use Zend\View\Model\JsonModel;

	class CalendarController extends AbstractActionController
	{
		protected $calendarTable;
		
		public function getCalendarTable()
		{
			if (!$this->calendarTable) {
				$sm = $this->getServiceLocator();
				$this->calendarTable = $sm->get('Calendar\Model\CalendarTable');
			}
			return $this->calendarTable;
		}
		
		public function indexAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				$id = (int) $this->params()->fromRoute('id', 0);
				
				// gdy nie podano id uzytkownika -> pokaz kalendarze osoby zalogowanej
				if (!$id)
				{
					$id = $this->zfcUserAuthentication()->getIdentity()->getId();
				}
				
				$calendars = $this->getCalendarTable()
						->fetchAllByUser($id);
				
				return array('calendars' => $calendars);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}

		public function createAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				$form = new CalendarForm();
				$form->get('user_id')->setValue($this->zfcUserAuthentication()->getIdentity()->getId());
				$form->get('submit')->setValue('Dodaj');

				$request = $this->getRequest();
				if ($request->isPost()) {
					$calendar = new Calendar();
					$form->setInputFilter($calendar->getInputFilter());
					$form->setData($request->getPost());

					if ($form->isValid()) {
						$calendar->exchangeArray($form->getData());
						$this->getCalendarTable()->saveCalendar($calendar);

						return $this->redirect()->toRoute('calendar');
					}
				}
				return array('form' => $form);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}
		
		public function showAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				$id = (int) $this->params()->fromRoute('id', 0);
				if (!$id) {
					return $this->redirect()->toRoute('calendar', array(
						'action' => 'create'
					));
				}
				
				$calendar = $this->getCalendarTable()->getCalendar($id);
				
				return array('calendar' => $calendar);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}

		public function settingsAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				$id = (int) $this->params()->fromRoute('id', 0);
				if (!$id) {
					return $this->redirect()->toRoute('calendar', array(
						'action' => 'create'
					));
				}
				
				$calendar = $this->getCalendarTable()->getCalendar($id);

				$form  = new CalendarForm();
				$form->bind($calendar);
				$form->get('submit')->setAttribute('value', 'Zapisz');

				$request = $this->getRequest();
				if ($request->isPost()) {
					$form->setInputFilter($calendar->getInputFilter());
					$form->setData($request->getPost());

					if ($form->isValid()) {
						$this->getCalendarTable()->saveCalendar($form->getData());

						return $this->redirect()->toRoute('calendar', array('action' => 'show', 'id' => $id));
					}
				}

				return array(
					'id' => $id,
					'form' => $form,
				);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}

		public function deleteAction()
		{
			if ($this->zfcUserAuthentication()->hasIdentity())
			{
				$id = (int) $this->params()->fromRoute('id', 0);
				if (!$id) {
					return $this->redirect()->toRoute('calendar');
				}

				$request = $this->getRequest();
				if ($request->isPost()) {
					$del = $request->getPost('del', 'Nie');

					if ($del == 'Tak') {
						$id = (int) $request->getPost('calendar_id');
						$this->getCalendarTable()->deleteCalendar($id);
					}

					return $this->redirect()->toRoute('calendar');
				}

				return array(
					'id'    => $id,
					'calendar' => $this->getCalendarTable()->getCalendar($id)
				);
			}
			else
			{
				$this->redirect()->toRoute('zfcuser/login');
			}
		}
	}