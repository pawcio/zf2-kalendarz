<?php
namespace Calendar\Form;

use Zend\Form\Form;

class EventForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('event');
        $this->setAttribute('method', 'post');
		
		$this->add(array(
            'name' => 'event_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'calendar_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
		$this->add(array(
            'name' => 'author_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Tytuł',
            ),
        ));
		$this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Opis',
            ),
        ));
		$this->add(array(
            'name' => 'begin',
            'attributes' => array(
                'type'  => 'ZendFormElementDateTime',
            ),
            'options' => array(
                'label' => 'Rozpoczęcie',
				'min' => '2001-01-01 00:00:00'
            ),
        ));
		$this->add(array(
            'name' => 'end',
            'attributes' => array(
                'type'  => 'ZendFormElementDateTime',
            ),
            'options' => array(
                'label' => 'Zakończenie',
            ),
        ));
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Zapisz',
                'id' => 'submitbutton',
            ),
        ));
    }
}