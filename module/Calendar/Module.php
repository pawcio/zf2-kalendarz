<?php
	namespace Calendar;

	use Calendar\Model\Calendar;
	use Calendar\Model\CalendarTable;
	use Calendar\Model\Event;
	use Calendar\Model\EventTable;
	use Zend\Db\ResultSet\ResultSet;
	use Zend\Db\TableGateway\TableGateway;

	class Module
	{
		public function getAutoloaderConfig()
		{
			return array(
				'Zend\Loader\ClassMapAutoloader' => array(
					__DIR__ . '/autoload_classmap.php',
				),
				'Zend\Loader\StandardAutoloader' => array(
					'namespaces' => array(
						__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
					),
				),
			);
		}

		public function getConfig()
		{
			return include __DIR__ . '/config/module.config.php';
		}
		
		public function getServiceConfig()
		{
			return array(
				'factories' => array(
					'Calendar\Model\CalendarTable' =>  function($sm) {
						$tableGateway = $sm->get('CalendarTableGateway');
						$table = new CalendarTable($tableGateway);
						return $table;
					},
					'CalendarTableGateway' => function ($sm) {
						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
						$resultSetPrototype = new ResultSet();
						$resultSetPrototype->setArrayObjectPrototype(new Calendar());
						return new TableGateway('calendar', $dbAdapter, null, $resultSetPrototype);
					},
					'Calendar\Model\EventTable' =>  function($sm) {
						$tableGateway = $sm->get('EventTableGateway');
						$table = new EventTable($tableGateway);
						return $table;
					},
					'EventTableGateway' => function ($sm) {
						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
						$resultSetPrototype = new ResultSet();
						$resultSetPrototype->setArrayObjectPrototype(new Event());
						return new TableGateway('event', $dbAdapter, null, $resultSetPrototype);
					},	
				),
			);
		}
	}
